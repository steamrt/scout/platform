# scout Platform images

This project provides container images for the Steam Runtime 1 'scout'
Platform. These images can be used to test games and other software
that has been compiled for the scout environment.

For general information about scout, please see the README in the
`steamrt/scout` branch of the `steamrt` package:
<https://gitlab.steamos.cloud/steamrt/steamrt/-/blob/steamrt/scout/README.md>.

## Developing software that runs in scout

Please see steamrt/scout/sdk> for details.

## Testing software that runs in scout

The primary way to test software that runs in scout is to install it
via Steam, and run it via Steam's
[`LD_LIBRARY_PATH`-based runtime][LDLP].

For games that are not yet available via Steam, you can run in the scout
environment using commands like:

```
$ ~/.steam/root/ubuntu12_32/steam-runtime/setup.sh
$ ~/.steam/root/ubuntu12_32/steam-runtime/run.sh ./mygame
```

If Steam is not installed, for example on a continuous-integration system,
the result of unpacking
<https://repo.steampowered.com/steamrt-images-scout/snapshots/latest-steam-client-general-availability/steam-runtime.tar.xz>
can be used instead of `~/.steam/root/ubuntu12_32/steam-runtime`.

Alternatively, libraries from the scout runtime are available in a
container image, known as the Platform image.

[LDLP]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/blob/master/docs/ld-library-path-runtime.md

## Differences between the normal Steam Runtime and these container images

Normally, the Steam Runtime will take many libraries from the host system,
notably graphics drivers and glibc.
This is necessary for compatibility reasons.

When using the Platform container images provided here with a conventional
container tool such as Docker or Podman, none of this is done.
Instead, the software will receive a "pure" scout Platform environment,
with the graphics drivers and supporting libraries from the Platform
image, which are extremely old (most are taken from Ubuntu 12.04,
released in 2012).
This is a very strict lowest-common-denominator runtime environment.
Software that works in the Platform environment should also work with the
full Steam Runtime environment, but a lot of software that makes
assumptions about the host operating system will not work in the pure
Platform environment.

The graphics drivers in the Platform image are very old, and are unlikely
to work well with recent hardware.
To work around this, it might be necessary to force use of
software rendering (`llvmpipe`) by setting the environment variable
`LIBGL_ALWAYS_SOFTWARE=1`.
The normal Steam Runtime avoids this problem by using graphics drivers
from the host system.

Ordinary container tools like Docker and Podman will usually not share
the user's real home directory with the container.
This is desirable for continuous integration and similar systems, but
not necessarily desirable for running games.
If shared directories or other shared state are required, please consult
your chosen container tool's documentation.

Similarly, ordinary container tools like Docker and Podman will usually
not share the real IPC sockets for X11, Wayland, D-Bus and similar protocols
with the container.
If sharing these sockets is required, please consult your chosen container
tool's documentation.

## OCI images for Docker and Podman <a name="oci"></a>

For Docker:

    sudo docker pull registry.gitlab.steamos.cloud/steamrt/scout/platform

or for [Podman](https://podman.io/):

    podman pull registry.gitlab.steamos.cloud/steamrt/scout/platform

or for [Toolbx](https://containertoolbx.org/):

    toolbox create -i registry.gitlab.steamos.cloud/steamrt/scout/platform scout-platform
    toolbox enter scout-platform

See steamrt/scout/sdk> for more details of how to use these images
with Docker or Podman, substituting `platform` for `sdk` in the image names.

## Tar archives for non-OCI environments <a name="tar"></a>

If you are not using the official Steam Runtime, then using Docker or
Podman is recommended, but the same content is also available in a flat
tar archive for use with non-OCI tools.

The `com.valvesoftware.SteamRuntime.Platform-amd64,i386-scout-runtime.tar.gz`
archives available from
<https://repo.steampowered.com/steamrt1/images/>
have the same content as these OCI images.
The `files/` directory in the archive should be renamed to `usr/`
and used as the `/usr` directory of a chroot or container.

The chroot or container will also need the usual compatibility symlinks
`/bin -> usr/bin`, `/lib -> usr/lib`, `/lib64 -> usr/lib64` and
`/sbin -> usr/sbin`.

For some uses of the chroot or container, the template `/etc` and `/var`
from the tar archive's `files/etc/` and `files/var/` will need to be
copied to the chroot or container's `/etc` and `/var`.

## 32-bit code

The `registry.gitlab.steamos.cloud/steamrt/scout/platform` image is
primarily 64-bit (`x86_64`), but also contains a complete set of 32-bit
runtime libraries, so it can run either 32- or 64-bit games.

The `com.valvesoftware.SteamRuntime.Platform-i386-scout-runtime.tar.gz`
archive is a 32-bit-only version for completeness.
We do not publish a 32-bit-only OCI image corresponding to this.

## apt packages

All of the packages that are supported for use in Steam games are included
in the Platform image and do not need to be downloaded separately.

To minimize its size, the Platform image does not include package
management tools such as ``apt`` and ``dpkg``.

See steamrt/scout/sdk> for more details of the apt repositories that
were used to build this image.

## Source code

Source code for all the packages that go into the OCI image can be found
in the appropriate subdirectory of
<https://repo.steampowered.com/steamrt1/images/>
(look in the `sources/` directory).
